$(document).ready(function() {
  var $ul5 = $('ul.level5');
  if ($ul5.length > 0) {
    $ul5.hide();
    var $toggleButton = $('<a href="#" class="">').text(' Commenti').click(function(e) {
      $ul5.toggle('slow');
      e.preventDefault();
    });
    var $div = $('<div class="commenti" style="float:none">').append($('<span>').text($ul5.find('li').length)).append($toggleButton);
    $('ul.level4 li:first').append($div);
  }
});