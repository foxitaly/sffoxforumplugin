$(document).ready(function() {
  var $ul = $('ul.level1');
  $('a.paginazione').click(function(e) {
    var $a = $(e.target);
    $.ajax({
      url:     $a.attr('href') + '&sf_format=json',
      success: function(res) {
        $.each(res.threads, function(i, thread) {
          var $li = $('<li class="' + (thread.isMod ? 'admin' : 'user') + ' moderate-' + thread.status + '">');
          $li.append($('<div class="author">').text('Creata da: ' + thread.nick + ' il ' + thread.date));
          $li.append($('<h4>').append($('<a href="' + thread.url + '" title="Leggi tutto">').text(thread.title)));
          $li.append($('<p>').text(thread.text));
          if (thread.approve !== '') {
            $li.append($('<a class="buttonAccetta" title="Accetta" href="' + thread.approve + '">').text('PUBBLICA'));
          }
          if (thread.reject !== '') {
            $li.append($('<a class="buttonRifiuta" title="Rifiuta" href="' + thread.reject + '">').text('DEPUBBLICA'));
          }
          $li.append($('<div class="clear">'));
          var $div = $('<div class="commenti">').append($('<span>').text(thread.numComm).after(' Commenti'));
          var $read = $('<a href="' + thread.url + '" class="btn left">').text('LEGGI E COMMENTA');

          if(thread.reply != null)
          {
            var $replyDiv = $('<div class="commenti">').append($('<span>').text(thread.reply.numComm).after(' Commenti'));
            var $replyRead = $('<div class="module140 right">').append($('<a href="' + thread.reply.url + '" class="btn left">').text('LEGGI E COMMENTA'));

            var $replyLi = $('<li class="' + (thread.reply.isMod ? 'admin' : 'user') + ' moderate-' + thread.reply.status + '">');
            $replyLi.append($('<div class="author">').text('Creata da: ' + thread.reply.nick + ' il ' + thread.reply.date));
            $replyLi.append($('<p>').text(thread.reply.text)).append($replyDiv).append($replyRead);
            if (thread.reply.approve !== '') {
              $replyLi.append($('<a class="buttonAccetta" title="Accetta" href="' + thread.reply.approve + '">').text('PUBBLICA'));
            }
            if (thread.reply.reject !== '') {
              $replyLi.append($('<a class="buttonRifiuta" title="Rifiuta" href="' + thread.reply.reject + '">').text('DEPUBBLICA'));
            }
            $replyLi.append($('<div class="clear">'));
  

            var $reply = $('<ul class="level2">').append($replyLi);


            $li.append($reply);
            //$li.append($('<ul class="home level1">').append($('<li>').append($div).append($read).append($reply).append($('<div class="clear">'))));
          }
          else
          {
            $li.append($('<ul class="home">').append($('<li>').append($div).append($read).append($('<div class="clear">'))));
          }

          $ul.append($li);
        });
        if (res.next === '') {
          $a.remove();
        } else {
          $a.attr('href', res.next);
        }
      }
    });
    e.preventDefault();
  });
});
