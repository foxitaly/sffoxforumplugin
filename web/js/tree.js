$(document).ready(function(){

  var enterLinks = [];
  var $checked = $('<img src="/sfPropelORMPlugin/images/tick.png" title="Checked" alt="Checked">');

  // str_repeat
  var repeat = function(string, n) {
    var a = [];
    while(a.length < n) {
      a.push(string);
    }
    return a.join('');
  };

  // toggle active/inactive
  var toggleActive = function(e) {
    var $a = $(e.target);
    $.ajax({
      url:     $a.attr('href') + '&sf_format=json',
      success: function(r) {
        if (r === 'OK') {
          var $img = $a.parents('tr').find('td.sf_admin_list_td_active img');
          if ($img.length > 0) {
            $img.remove();
            $a.text('activate');
          } else {
            $a.parents('tr').find('td.sf_admin_list_td_active').append($checked.clone());
            $a.text('deactivate');
          }
        }
      }
    });
    e.preventDefault();
  };

  // remove a node and its children
  var removeChildren = function($tr) {
    var $children = $('tr.childof_' + $tr.attr('id').substr(4));
    $children.each(function(i, tr) {
      removeChildren($(tr));
    });
    $tr.remove();
  };

  // toggle a node
  var toggle = function(e) {
    var $a = $(e.target);
    var status = $a.attr('class');
    var id = $a.attr('id').substr(3);
    var $tr = $('#row_' + id);
    if ($tr.length > 0) {
      //var fa = $tr.find('a:first');
      if (status === 'closed') { // expand closed row
        $.ajax({
          url:     enterLinks['row_' + id] + '&sf_format=json',
          success: function(r) {
            $.each(r, function(i, child) {
              var $link = $('<a class="closed" href="#" id="id_' + child.Id + '" title="expand">').text('+').click(toggle);
              var $child = $('<tr id="row_' + child.Id + '" class="childof_' + id + '">');
              var $td0 = $('<td>').append(repeat('&nbsp;', child.TreeLevel));
              if (child.hasChildren) {
                $td0.append($link);
                enterLinks['row_' + child.Id] = enterLinks['row_' + id].replace(id, child.Id);
              }
              $child.append($td0);
              $child.append($('<td class="sf_admin_text">').append(repeat('&nbsp; &nbsp;', child.TreeLevel) + child.Message));
              $child.append($('<td class="sf_admin_text">').text(child.Nickname));
              $child.append($('<td class="sf_admin_text">').text(child.Ip + ' '));
              $child.append($('<td class="sf_admin_boolean">').append(child.Moderator ? $checked.clone() : ''));
              $child.append($('<td class="sf_admin_boolean sf_admin_list_td_active">').append(child.Active ? $checked.clone() : ''));
              $child.append($('<td class="sf_admin_date">').text(child.CreatedAt));
              $child.append($('<td class="sf_admin_date">').text(child.UpdatedAt));
              var $ul = $tr.find('ul.sf_admin_td_actions').clone();
              var $ta = $ul.find('a');
              if (child.Active) {
                $ta.text('deactivate');
              } else {
                $ta.text('activate');
              }
              $ta.click(toggleActive).attr('href', $ta.attr('href').replace(id, child.Id));
              $child.append($('<td>').append($ul));
              $tr.after($child);
            });
            $a.attr('class', 'open').text('-').attr('title', 'collapse');
          }
        });
      } else {  // close epxanded row
        $children = $('tr.childof_' + $tr.attr('id').substr(4));
        $children.each(function(i, tr) {
          removeChildren($(tr));
        });
        $a.attr('class', 'closed').text('+');
      }
    }
    e.preventDefault();
  };

  // main code
  $('thead input').remove();
  var $trs = $('tbody tr');
  $trs.each(function(i, tr) {
    var $tr = $(tr);
    // checkbox in first td
    var $td1 = $tr.find('td:first');
    var $cb = $td1.find('input[type="checkbox"]:first');
    var id = $cb.val();
    $cb.remove();
    // link to expand/collapse node
    if ($tr.hasClass('hc')) { // hc = has children
      var $link = $('<a class="closed" href="#" id="id_' + id + '" title="expand">').text('+').click(toggle);
      $td1.append($link);
    }
    enterLinks[$tr.attr('id')] = $tr.find('li.sf_admin_action_enter a').attr('href');
    // remove "enter" link
    var $lis = $tr.find('li');
    $lis.each(function(i, li) {
      var $li = $(li);
      if ($li.hasClass('sf_admin_action_enter')) {
        var $newLi = $('<li>');
        var $a = $('<a href="#TODO">').text('new');
        $li.append($a);
        $li.parent('ul').append($newLi);
        $li.remove();
      } else if ($li.hasClass('sf_admin_action_toggle_active')) {
        $li.find('a').click(toggleActive);
      }
    });
  });

});
