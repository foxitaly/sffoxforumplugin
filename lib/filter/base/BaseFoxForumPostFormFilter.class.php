<?php

/**
 * FoxForumPost filter form base class.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage filter
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseFoxForumPostFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'thread_id'  => new sfWidgetFormPropelChoice(array('model' => 'FoxForumThread', 'add_empty' => true)),
      'guid'       => new sfWidgetFormFilterInput(),
      'message'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nickname'   => new sfWidgetFormFilterInput(),
      'email'      => new sfWidgetFormFilterInput(),
      'ip'         => new sfWidgetFormFilterInput(),
      'moderator'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'active'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'tree_left'  => new sfWidgetFormFilterInput(),
      'tree_right' => new sfWidgetFormFilterInput(),
      'tree_level' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'thread_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'FoxForumThread', 'column' => 'id')),
      'guid'       => new sfValidatorPass(array('required' => false)),
      'message'    => new sfValidatorPass(array('required' => false)),
      'nickname'   => new sfValidatorPass(array('required' => false)),
      'email'      => new sfValidatorPass(array('required' => false)),
      'ip'         => new sfValidatorPass(array('required' => false)),
      'moderator'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'active'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'tree_left'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tree_right' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tree_level' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('fox_forum_post_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FoxForumPost';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'thread_id'  => 'ForeignKey',
      'guid'       => 'Text',
      'message'    => 'Text',
      'nickname'   => 'Text',
      'email'      => 'Text',
      'ip'         => 'Text',
      'moderator'  => 'Boolean',
      'active'     => 'Boolean',
      'created_at' => 'Date',
      'updated_at' => 'Date',
      'tree_left'  => 'Number',
      'tree_right' => 'Number',
      'tree_level' => 'Number',
    );
  }
}
