<?php

/**
 * FoxForumThread form.
 *
 * @package    Fox
 * @subpackage form
 * @author     Massimiliano Arione
 */
class FoxForumThreadForm extends BaseFoxForumThreadForm
{
  protected $user;

  public function configure()
  {
    // TODO forse qualcuna di queste serve in backend...
    unset($this['thread_id'], $this['guid'], $this['nickname'], $this['email'],
          $this['ip'], $this['moderator'], $this['status'], $this['created_at'],
          $this['updated_at']);

    $this->user = $this->options['user'];

  }

  /**
   * Save a new thread and then set correct properties for it
   *
   * @param string    $ip
   * @param PropelPDO $con
   */
  public function insertNew($ip, PropelPDO $con = null)
  {
    if ($this->isBound())
    {
      $thread = $this->save($con);
      $thread
        ->setEmail($this->user->getEmail())->setNickname($this->user->getNickname())->setGuid($this->user->getUid())
        ->setModerator($this->user->isModerator())->setIp($ip);
      if ($this->user->isModerator())
      {
        $thread->approve();
      }
      $thread->save($con);

      return $thread;
    }
  }
}
