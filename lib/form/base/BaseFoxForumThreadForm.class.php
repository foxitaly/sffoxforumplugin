<?php

/**
 * FoxForumThread form base class.
 *
 * @method FoxForumThread getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseFoxForumThreadForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'guid'         => new sfWidgetFormInputText(),
      'title'        => new sfWidgetFormInputText(),
      'message'      => new sfWidgetFormTextarea(),
      'nickname'     => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'ip'           => new sfWidgetFormInputText(),
      'moderator'    => new sfWidgetFormInputCheckbox(),
      'status'       => new sfWidgetFormInputText(),
      'notify_email' => new sfWidgetFormInputCheckbox(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'slug'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'guid'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'title'        => new sfValidatorString(array('max_length' => 255)),
      'message'      => new sfValidatorString(),
      'nickname'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ip'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'moderator'    => new sfValidatorBoolean(array('required' => false)),
      'status'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'notify_email' => new sfValidatorBoolean(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
      'slug'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'FoxForumThread', 'column' => array('slug')))
    );

    $this->widgetSchema->setNameFormat('fox_forum_thread[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FoxForumThread';
  }


}
