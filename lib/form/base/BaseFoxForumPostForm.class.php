<?php

/**
 * FoxForumPost form base class.
 *
 * @method FoxForumPost getObject() Returns the current form's model object
 *
 * @package    ##PROJECT_NAME##
 * @subpackage form
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseFoxForumPostForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'thread_id'    => new sfWidgetFormPropelChoice(array('model' => 'FoxForumThread', 'add_empty' => false)),
      'guid'         => new sfWidgetFormInputText(),
      'message'      => new sfWidgetFormTextarea(),
      'nickname'     => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'ip'           => new sfWidgetFormInputText(),
      'moderator'    => new sfWidgetFormInputCheckbox(),
      'status'       => new sfWidgetFormInputText(),
      'notify_email' => new sfWidgetFormInputCheckbox(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
      'tree_left'    => new sfWidgetFormInputText(),
      'tree_right'   => new sfWidgetFormInputText(),
      'tree_level'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'thread_id'    => new sfValidatorPropelChoice(array('model' => 'FoxForumThread', 'column' => 'id')),
      'guid'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'message'      => new sfValidatorString(),
      'nickname'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ip'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'moderator'    => new sfValidatorBoolean(array('required' => false)),
      'status'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'notify_email' => new sfValidatorBoolean(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
      'tree_left'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'tree_right'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
      'tree_level'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fox_forum_post[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FoxForumPost';
  }


}
