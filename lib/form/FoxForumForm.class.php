<?php

/**
 * FoxForum form.
 *
 * @package    Fox
 * @subpackage form
 * @author     Massimiliano Arione
 */
class FoxForumForm extends BaseFoxForumForm
{
  public function configure()
  {
    unset($this['id']);

    $this->widgetSchema['logo'] = new sfWidgetFormInputFileEditable(array(
      'file_src'    => '/uploads/photo/' . $this->getObject()->getLogo(),
      'is_image'    => true,
      'with_delete' => false,
    ));
    $this->widgetSchema['logo']->setLabel('Logo <br />(<strong style="color:blue">250x250px</strong>)');

    $this->validatorSchema['logo'] = new sfImageFileValidator(array(
      'required'   => false,
      'path'       => sfConfig::get('sf_upload_dir') . '/photo',
      'mime_types' => 'web_images',
      'min_height' => 250,
      'min_width'  => 250,
      'max_height' => 250,
      'max_width'  => 250,
    ), array(
      'min_height'  => 'The image must be 250x250 px',
      'max_height'  => 'The image must be 250x250 px',
      'min_width'   => 'The image must be 250x250 px',
      'max_width'   => 'The image must be 250x250 px',
    ));
  }
}