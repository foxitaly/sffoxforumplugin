<?php

/**
 * FoxForumPost form.
 *
 * @package    Fox
 * @subpackage form
 * @author     Massimiliano Arione
 */
class FoxForumPostForm extends BaseFoxForumPostForm
{
  public function configure()
  {
    unset($this['id'], $this['guid'], $this['tree_left'], $this['tree_right'],
          $this['tree_level'], $this['created_at'], $this['updated_at']);
    // in frontend, remove some more fields
    if (!empty($this->options['user']))
    {
      unset($this['thread_id'], $this['guid'], $this['nickname'], $this['email'],
            $this['ip'], $this['moderator'], $this['status']);
    }
    else
    {
      $this->widgetSchema['thread_id']->setOption('connection', Propel::getConnection('sostataforum'));
      $this->widgetSchema['thread_id']->setOption('query_methods', array('orderByTitle'));
      $this->validatorSchema['thread_id']->setOption('connection', Propel::getConnection('sostataforum'));
    }

  }

  /**
   * Save a new post as reply (= first post)
   *
   * @param string         $ip
   * @param FoxForumThread $thread
   * @param PropelPDO      $con
   */
  public function insertReply($ip, FoxForumThread $thread, PropelPDO $con = null)
  {
    $user = $this->options['user'];
    if ($this->isBound())
    {
      if (!$user->isModerator() && !$thread->getModerator())
      {
        throw new DomainException('only moderators can insert replies');
      }

      $post = $this->getObject();
      $this->updateObject();
      $post->setThread($thread);
      if($user->isModerator())
      {
        $post->setModerator(true)->approve();
      }
      $post->setEmail($user->getEmail())->setNickname($user->getNickname())->setGuid($user->getUid())
      ->setIp($ip)->makeRoot();
      $post->save($con);
      return $post;
    }
  }

  /**
   * Save a new post as comment
   *
   * @param string       $ip
   * @param FoxForumPost $post
   * @param PropelPDO    $con
   */
  public function insertComment($ip, FoxForumPost $post, PropelPDO $con = null)
  {
    $user = $this->options['user'];
    if ($this->isBound())
    {
      if (!$post->getModerator() && !$user->isModerator())
      {
        throw new DomainException('only moderators can reply to users');
      }
      if ($post->getTreeLevel() > 2)
      {
        throw new DomainException('cannot reply after 2th level');
      }
      $comment = $this->getObject();
      $this->updateObject();
      $comment->setThread($post->getThread($con))->setIp($ip)
        ->setEmail($user->getEmail())->setNickname($user->getNickname())->setGuid($user->getUid())
        ->setModerator($user->isModerator())
        ->insertAsLastChildOf($post);
      if ($user->isModerator())
      {
        $comment->approve();
      }
      $comment->save($con);
      return $comment;
    }
  }
}
