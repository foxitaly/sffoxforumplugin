<?php


/**
 * Base class that represents a query for the 'fox_forum_thread' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Nov 11 19:18:03 2013
 *
 * @method FoxForumThreadQuery orderById($order = Criteria::ASC) Order by the id column
 * @method FoxForumThreadQuery orderByGuid($order = Criteria::ASC) Order by the guid column
 * @method FoxForumThreadQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method FoxForumThreadQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method FoxForumThreadQuery orderByNickname($order = Criteria::ASC) Order by the nickname column
 * @method FoxForumThreadQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method FoxForumThreadQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method FoxForumThreadQuery orderByModerator($order = Criteria::ASC) Order by the moderator column
 * @method FoxForumThreadQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method FoxForumThreadQuery orderByNotifyEmail($order = Criteria::ASC) Order by the notify_email column
 * @method FoxForumThreadQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method FoxForumThreadQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method FoxForumThreadQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 *
 * @method FoxForumThreadQuery groupById() Group by the id column
 * @method FoxForumThreadQuery groupByGuid() Group by the guid column
 * @method FoxForumThreadQuery groupByTitle() Group by the title column
 * @method FoxForumThreadQuery groupByMessage() Group by the message column
 * @method FoxForumThreadQuery groupByNickname() Group by the nickname column
 * @method FoxForumThreadQuery groupByEmail() Group by the email column
 * @method FoxForumThreadQuery groupByIp() Group by the ip column
 * @method FoxForumThreadQuery groupByModerator() Group by the moderator column
 * @method FoxForumThreadQuery groupByStatus() Group by the status column
 * @method FoxForumThreadQuery groupByNotifyEmail() Group by the notify_email column
 * @method FoxForumThreadQuery groupByCreatedAt() Group by the created_at column
 * @method FoxForumThreadQuery groupByUpdatedAt() Group by the updated_at column
 * @method FoxForumThreadQuery groupBySlug() Group by the slug column
 *
 * @method FoxForumThreadQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method FoxForumThreadQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method FoxForumThreadQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method FoxForumThreadQuery leftJoinPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the Post relation
 * @method FoxForumThreadQuery rightJoinPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Post relation
 * @method FoxForumThreadQuery innerJoinPost($relationAlias = null) Adds a INNER JOIN clause to the query using the Post relation
 *
 * @method FoxForumThread findOne(PropelPDO $con = null) Return the first FoxForumThread matching the query
 * @method FoxForumThread findOneOrCreate(PropelPDO $con = null) Return the first FoxForumThread matching the query, or a new FoxForumThread object populated from the query conditions when no match is found
 *
 * @method FoxForumThread findOneByGuid(string $guid) Return the first FoxForumThread filtered by the guid column
 * @method FoxForumThread findOneByTitle(string $title) Return the first FoxForumThread filtered by the title column
 * @method FoxForumThread findOneByMessage(string $message) Return the first FoxForumThread filtered by the message column
 * @method FoxForumThread findOneByNickname(string $nickname) Return the first FoxForumThread filtered by the nickname column
 * @method FoxForumThread findOneByEmail(string $email) Return the first FoxForumThread filtered by the email column
 * @method FoxForumThread findOneByIp(string $ip) Return the first FoxForumThread filtered by the ip column
 * @method FoxForumThread findOneByModerator(boolean $moderator) Return the first FoxForumThread filtered by the moderator column
 * @method FoxForumThread findOneByStatus(int $status) Return the first FoxForumThread filtered by the status column
 * @method FoxForumThread findOneByNotifyEmail(boolean $notify_email) Return the first FoxForumThread filtered by the notify_email column
 * @method FoxForumThread findOneByCreatedAt(string $created_at) Return the first FoxForumThread filtered by the created_at column
 * @method FoxForumThread findOneByUpdatedAt(string $updated_at) Return the first FoxForumThread filtered by the updated_at column
 * @method FoxForumThread findOneBySlug(string $slug) Return the first FoxForumThread filtered by the slug column
 *
 * @method array findById(int $id) Return FoxForumThread objects filtered by the id column
 * @method array findByGuid(string $guid) Return FoxForumThread objects filtered by the guid column
 * @method array findByTitle(string $title) Return FoxForumThread objects filtered by the title column
 * @method array findByMessage(string $message) Return FoxForumThread objects filtered by the message column
 * @method array findByNickname(string $nickname) Return FoxForumThread objects filtered by the nickname column
 * @method array findByEmail(string $email) Return FoxForumThread objects filtered by the email column
 * @method array findByIp(string $ip) Return FoxForumThread objects filtered by the ip column
 * @method array findByModerator(boolean $moderator) Return FoxForumThread objects filtered by the moderator column
 * @method array findByStatus(int $status) Return FoxForumThread objects filtered by the status column
 * @method array findByNotifyEmail(boolean $notify_email) Return FoxForumThread objects filtered by the notify_email column
 * @method array findByCreatedAt(string $created_at) Return FoxForumThread objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return FoxForumThread objects filtered by the updated_at column
 * @method array findBySlug(string $slug) Return FoxForumThread objects filtered by the slug column
 *
 * @package    propel.generator.plugins.sfFoxForumPlugin.lib.model.om
 */
abstract class BaseFoxForumThreadQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseFoxForumThreadQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'propel', $modelName = 'FoxForumThread', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new FoxForumThreadQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   FoxForumThreadQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return FoxForumThreadQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof FoxForumThreadQuery) {
            return $criteria;
        }
        $query = new FoxForumThreadQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   FoxForumThread|FoxForumThread[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FoxForumThreadPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(FoxForumThreadPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FoxForumThread A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 FoxForumThread A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `guid`, `title`, `message`, `nickname`, `email`, `ip`, `moderator`, `status`, `notify_email`, `created_at`, `updated_at`, `slug` FROM `fox_forum_thread` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
      $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new FoxForumThread();
            $obj->hydrate($row);
            FoxForumThreadPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return FoxForumThread|FoxForumThread[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|FoxForumThread[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FoxForumThreadPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FoxForumThreadPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FoxForumThreadPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FoxForumThreadPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the guid column
     *
     * Example usage:
     * <code>
     * $query->filterByGuid('fooValue');   // WHERE guid = 'fooValue'
     * $query->filterByGuid('%fooValue%'); // WHERE guid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $guid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByGuid($guid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($guid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $guid)) {
                $guid = str_replace('*', '%', $guid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::GUID, $guid, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $message)) {
                $message = str_replace('*', '%', $message);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the nickname column
     *
     * Example usage:
     * <code>
     * $query->filterByNickname('fooValue');   // WHERE nickname = 'fooValue'
     * $query->filterByNickname('%fooValue%'); // WHERE nickname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nickname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByNickname($nickname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nickname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nickname)) {
                $nickname = str_replace('*', '%', $nickname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::NICKNAME, $nickname, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%'); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ip)) {
                $ip = str_replace('*', '%', $ip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::IP, $ip, $comparison);
    }

    /**
     * Filter the query on the moderator column
     *
     * Example usage:
     * <code>
     * $query->filterByModerator(true); // WHERE moderator = true
     * $query->filterByModerator('yes'); // WHERE moderator = true
     * </code>
     *
     * @param     boolean|string $moderator The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByModerator($moderator = null, $comparison = null)
    {
        if (is_string($moderator)) {
            $moderator = in_array(strtolower($moderator), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FoxForumThreadPeer::MODERATOR, $moderator, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status >= 12
     * $query->filterByStatus(array('max' => 12)); // WHERE status <= 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(FoxForumThreadPeer::STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(FoxForumThreadPeer::STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the notify_email column
     *
     * Example usage:
     * <code>
     * $query->filterByNotifyEmail(true); // WHERE notify_email = true
     * $query->filterByNotifyEmail('yes'); // WHERE notify_email = true
     * </code>
     *
     * @param     boolean|string $notifyEmail The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByNotifyEmail($notifyEmail = null, $comparison = null)
    {
        if (is_string($notifyEmail)) {
            $notifyEmail = in_array(strtolower($notifyEmail), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FoxForumThreadPeer::NOTIFY_EMAIL, $notifyEmail, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FoxForumThreadPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FoxForumThreadPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FoxForumThreadPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FoxForumThreadPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%'); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $slug)) {
                $slug = str_replace('*', '%', $slug);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoxForumThreadPeer::SLUG, $slug, $comparison);
    }

    /**
     * Filter the query by a related FoxForumPost object
     *
     * @param   FoxForumPost|PropelObjectCollection $foxForumPost  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 FoxForumThreadQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPost($foxForumPost, $comparison = null)
    {
        if ($foxForumPost instanceof FoxForumPost) {
            return $this
                ->addUsingAlias(FoxForumThreadPeer::ID, $foxForumPost->getThreadId(), $comparison);
        } elseif ($foxForumPost instanceof PropelObjectCollection) {
            return $this
                ->usePostQuery()
                ->filterByPrimaryKeys($foxForumPost->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPost() only accepts arguments of type FoxForumPost or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Post relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function joinPost($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Post');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Post');
        }

        return $this;
    }

    /**
     * Use the Post relation FoxForumPost object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   FoxForumPostQuery A secondary query class using the current class as primary query
     */
    public function usePostQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Post', 'FoxForumPostQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   FoxForumThread $foxForumThread Object to remove from the list of results
     *
     * @return FoxForumThreadQuery The current query, for fluid interface
     */
    public function prune($foxForumThread = null)
    {
        if ($foxForumThread) {
            $this->addUsingAlias(FoxForumThreadPeer::ID, $foxForumThread->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

  // timestampable behavior

  /**
   * Filter by the latest updated
   *
   * @param      int $nbDays Maximum age of the latest update in days
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function recentlyUpdated($nbDays = 7)
  {
      return $this->addUsingAlias(FoxForumThreadPeer::UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
  }

  /**
   * Order by update date desc
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function lastUpdatedFirst()
  {
      return $this->addDescendingOrderByColumn(FoxForumThreadPeer::UPDATED_AT);
  }

  /**
   * Order by update date asc
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function firstUpdatedFirst()
  {
      return $this->addAscendingOrderByColumn(FoxForumThreadPeer::UPDATED_AT);
  }

  /**
   * Filter by the latest created
   *
   * @param      int $nbDays Maximum age of in days
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function recentlyCreated($nbDays = 7)
  {
      return $this->addUsingAlias(FoxForumThreadPeer::CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
  }

  /**
   * Order by create date desc
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function lastCreatedFirst()
  {
      return $this->addDescendingOrderByColumn(FoxForumThreadPeer::CREATED_AT);
  }

  /**
   * Order by create date asc
   *
   * @return     FoxForumThreadQuery The current query, for fluid interface
   */
  public function firstCreatedFirst()
  {
      return $this->addAscendingOrderByColumn(FoxForumThreadPeer::CREATED_AT);
  }
  // sluggable behavior

  /**
   * Find one object based on its slug
   *
   * @param     string $slug The value to use as filter.
   * @param     PropelPDO $con The optional connection object
   *
   * @return    FoxForumThread the result, formatted by the current formatter
   */
  public function findOneBySlug($slug, $con = null)
  {
      return $this->filterBySlug($slug)->findOne($con);
  }

}
