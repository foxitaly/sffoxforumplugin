<?php

/**
 * Questa estensione della view di symfony serve per poter usare
 * i template del modulo sfFoxForumFrontend senza doverli
 * necessariamene ridefinire nel modulo chiamante
 *
 * Specificare in config/module.yml del modulo chiamante:
 *   view_class:  foxForum
 *
 */

class foxForumView extends sfPHPView
{
  protected $pluginDirectory;

  /**
   * Constructor
   */
  public function __construct($context, $moduleName, $actionName, $viewName)
  {
    parent::__construct($context, $moduleName, $actionName, $viewName);
#var_dump($moduleName, sfConfig::get('mod_' . strtolower($moduleName) . '_module'));die;
    $module = sfConfig::get('mod_' . strtolower($moduleName) . '_module');

    $this->pluginDirectory = dirname(__FILE__) . '/../../modules/' . $module . '/templates/';
  }

  /**
   * Renders the presentation, falling back on plugin directory if template
   * is not found in calling module
   *
   * @param  string $_sfFile  Filename
   * @return string File content
   */
  protected function renderFile($_sfFile)
  {
    try
    {
      $string = parent::renderFile($_sfFile);
    }
    catch (sfRenderException $e)
    {
      $string = parent::renderFile($this->pluginDirectory . $_sfFile);
    }

    return $string;
  }

  /**
   * If standard preRenderCheck fails, try it with plugin directory
   *
   * @throws sfRenderException If the pre-render check fails
   */
  protected function preRenderCheck()
  {
    try
    {
      parent::preRenderCheck();
    }
    catch (sfRenderException $e)
    {
      $this->directory = $this->pluginDirectory;
      parent::preRenderCheck();
    }
  }
}