<?php

require_once dirname(__FILE__).'/../lib/BasesfFoxForumBackendActions.class.php';

/**
 * sfFoxForumBackend actions.
 *
 * @package    sfFoxForumPlugin
 * @subpackage sfFoxForumBackend
 * @author     Consulting Alpha
 * @version    SVN: $Id: actions.class.php 1432 2012-12-06 13:28:30Z garakkio $
 */
class sfFoxForumBackendActions extends BasesfFoxForumBackendActions
{
  public function preExecute()
  {
    parent::preExecute();
    $pattern = $this->getRoute()->getPattern();
    $this->route = substr($pattern, 1, strpos($pattern, '.') - 1);
    sfConfig::set('mod_route', $this->route);
    $options = $this->getRoute()->getOptions();
    $this->connection = Propel::getConnection($options['connection']);
  }

  /**
   * subtree
   */
  public function executeEnter(sfWebRequest $request)
  {
    $this->sort = null;
    $this->post = FoxForumPostQuery::create()->findPk($request->getParameter('id'), $this->connection);
    $this->forward404Unless($this->post, 'post not found');
    $this->pager = $this->post->getChildren(null, $this->connection);
    $this->setTemplate('index');
    $this->conn = $this->connection;  // XXX non prende direttamente $connection :-|
  }

  /**
   * activate/deactivate post
   */
  public function executeToggleActive(sfWebRequest $request)
  {
    $post = FoxForumPostQuery::create()->findPk($request->getParameter('id'), $this->connection);
    $this->forward404Unless($post, 'post not found');
    $post->setActive(!$post->getActive())->save($this->connection);
    $this->redirectUnless($request->isXmlHttpRequest(), '@' . $this->route);

    return $this->renderText('OK');
  }

  protected function getPager()
  {
    $query = $this->buildQuery();
    $paginateMethod = $this->configuration->getPaginateMethod();
    $pager = $query->$paginateMethod($this->getPage(), $this->configuration->getPagerMaxPerPage(), $this->connection);

    return $pager;
  }
}
