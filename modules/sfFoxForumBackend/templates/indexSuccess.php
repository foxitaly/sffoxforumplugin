<?php include_partial('sfFoxForumBackend/assets') ?>
<?php use_javascript('/sfFoxForumPlugin/js/tree.js') ?>

<div id="sf_admin_container">
  <?php if (empty($post)): ?>
  <h1>Posts</h1>
  <?php else: ?>
  <h1>Posts: <?php echo $post ?> </h1>
  <?php $parent = $post->getParent($conn->getRawValue()) ?>
  <?php echo $parent ? link_to(__('Up'), 'sfFoxForumBackend/enter?id=' . $parent->getId()) : '' ?>
  <?php endif ?>

  <?php include_partial('sfFoxForumBackend/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('sfFoxForumBackend/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for(sfConfig::get('mod_route') . '_collection', array('action' => 'batch')) ?>" method="post">
    <?php include_partial('sfFoxForumBackend/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <?php if (!empty($post)): ?>
    <ul class="sf_admin_actions">
      <?php include_partial('sfFoxForumBackend/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('sfFoxForumBackend/list_actions', array('helper' => $helper, 'fox_forum_post' => $post)) ?>
    </ul>
    <?php endif ?>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('sfFoxForumBackend/list_footer', array('pager' => $pager)) ?>
  </div>

</div>
