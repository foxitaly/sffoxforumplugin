<td>
  <ul class="sf_admin_td_actions">
    <?php echo $fox_forum_post->isRoot() ? '' : $helper->linkToEdit($fox_forum_post, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
    <?php echo $fox_forum_post->isRoot() ? '' : $helper->linkToDelete($fox_forum_post, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
    <?php if (!$fox_forum_post->isLeaf()): ?>
      <?php if ($fox_forum_post->getId() != $sf_request->getParameter('id')): ?>
        <li class="sf_admin_action_enter">
          <?php echo link_to('Enter', '@' . sfConfig::get('mod_route') . '_enter?id=' . $fox_forum_post->getId()) ?>
        </li>
      <?php endif ?>
    <?php endif ?>
    <li class="sf_admin_action_toggle_active">
      <?php echo link_to($fox_forum_post->getActive()? 'deactivate' : 'activate', '@' . sfConfig::get('mod_route') . '_toggleActive?id=' . $fox_forum_post->getId()) ?>
    </li>
  </ul>
</td>
