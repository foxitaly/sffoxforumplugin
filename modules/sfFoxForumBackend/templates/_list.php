<div class="sf_admin_list">
  <?php if ($pager->count() == 0): ?>
    <p><?php echo __('No result', array(), 'sf_admin') ?></p>
  <?php else: ?>
    <table cellspacing="0">
      <thead>
        <tr>
          <th id="sf_admin_list_batch_actions"><input id="sf_admin_list_batch_checkbox" type="checkbox" onclick="checkAll();" /></th>
          <?php include_partial('sfFoxForumBackend/list_th_tabular', array('sort' => $sort)) ?>
          <th id="sf_admin_list_th_actions"><?php echo __('Actions', array(), 'sf_admin') ?></th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th colspan="9">
          </th>
        </tr>
      </tfoot>
      <tbody>
        <?php foreach ($pager as $i => $post): ?>
          <?php $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
          <tr id="row_<?php echo $post->getId() ?>" class="sf_admin_row <?php echo $odd ?> level_<?php echo $post->getLevel() ?> <?php echo $post->hasChildren() ? 'hc' : 'nc' ?>">
            <?php include_partial('sfFoxForumBackend/list_td_batch_actions', array('fox_forum_post' => $post, 'helper' => $helper)) ?>
            <?php include_partial('sfFoxForumBackend/list_td_tabular', array('fox_forum_post' => $post)) ?>
            <?php include_partial('sfFoxForumBackend/list_td_actions', array('fox_forum_post' => $post, 'helper' => $helper)) ?>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  <?php endif ?>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(index in boxes) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>