<?php

$array = array();

foreach ($pager as $child)
{
  $c = $child->toArray()->getRawValue();
  $array[] = $c + array('hasChildren' => $child->hasChildren());
}

echo json_encode($array);