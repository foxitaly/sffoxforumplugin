<?php

/**
 * sfFoxForumBackend module configuration.
 *
 * @package    fox
 * @subpackage sfFoxForum
 * @author     Massimiliano Arione
 * @version    SVN: $Id: sfFoxForumBackendGeneratorConfiguration.class.php 1393 2012-12-03 16:57:38Z garakkio $
 */
class sfFoxForumBackendGeneratorConfiguration extends BaseSfFoxForumBackendGeneratorConfiguration
{
}
