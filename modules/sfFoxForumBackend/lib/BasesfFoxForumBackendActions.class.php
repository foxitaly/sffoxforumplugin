<?php

require_once dirname(__FILE__).'/sfFoxForumBackendGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/sfFoxForumBackendGeneratorHelper.class.php';

/**
 * Base actions for the sfFoxForumPlugin sfFoxForumBackend module.
 *
 * @package     sfFoxForumPlugin
 * @subpackage  sfFoxForumBackend
 * @author      Consulting Alpha
 * @version     SVN: $Id: BasesfFoxForumBackendActions.class.php 1456 2012-12-07 09:08:45Z root $
 */
class BasesfFoxForumBackendActions extends autoSfFoxForumBackendActions
{
  protected $connection;

  /*
  public function preExecute()
  {

    $this->connection = Propel::getConnection('sostataforum');

    parent::preExecute();
  }
  */
}
