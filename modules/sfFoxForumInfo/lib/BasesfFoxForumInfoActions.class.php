<?php

/**
 * Base actions for the sfFoxForumPlugin sfFoxForumInfo module.
 *
 * @package     sfFoxForumPlugin
 * @subpackage  sfFoxForumInfo
 * @author      Consulting Alpha
 * @version     SVN: $Id: BasesfFoxForumInfoActions.class.php 1476 2012-12-10 10:42:46Z garakkio $
 */
class BasesfFoxForumInfoActions extends sfActions
{
  public function preExecute()
  {
    $pattern = $this->getRoute()->getPattern();
    if (strpos($pattern, '/', 1) > 0)
    {
      // e.g. '/cambiocane_forum/:id/edit.:sf_format'
      $this->route = substr($pattern, 1, strpos($pattern, '/', 1) - 1);
    }
    else
    {
      // e.g. '/cambiocane_forum.:sf_format'
      $this->route = substr($pattern, 1, strpos($pattern, '.') - 1);
    }
    $options = $this->getRoute()->getOptions();
    $this->connection = Propel::getConnection($options['connection']);
  }

  /**
   * Redirect to edit
   */
  public function executeIndex(sfWebRequest $request)
  {
    $forum = FoxForumQuery::create()->findOne($this->connection);
    $this->forward404Unless($forum, 'forum not found');
    $this->redirect('@' . $this->route . '_edit?id=' . $forum->getId());
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forum = $this->getRoute()->getObject();
    $this->form = new FoxForumForm($this->forum);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forum = $this->getRoute()->getObject();
    $this->form = new FoxForumForm($this->forum);
    $this->processForm($request, $this->form);
    $this->setTemplate('edit', 'sfFoxForumInfo');
  }

  /**
   * Save with connection
   */
  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = 'The item was updated successfully.';
      $forum = $form->save($this->connection);
      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $forum)));
      $this->getUser()->setFlash('notice', $notice);
      $this->redirect(array('sf_route' => $this->route . '_edit', 'sf_subject' => $forum));
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
}
