<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <form action="<?php echo url_for($route . '_update', $forum) ?>" method="post" enctype="multipart/form-data">
    <fieldset id="sf_fieldset_none">

      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_title">
        <?php echo $form['title']->renderError() ?>
        <div>
          <?php echo $form['title']->renderLabel() ?>
          <div class="content">
            <?php echo $form['title']->render() ?>
          </div>
        </div>
      </div>

      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_subtitle">
        <?php echo $form['subtitle']->renderError() ?>
        <div>
          <?php echo $form['subtitle']->renderLabel() ?>
          <div class="content">
            <?php echo $form['subtitle']->render() ?>
          </div>
        </div>
      </div>

      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_name">
        <?php echo $form['warning']->renderError() ?>
        <div>
          <?php echo $form['warning']->renderLabel() ?>
          <div class="content">
            <?php echo $form['warning']->render() ?>
          </div>
        </div>
      </div>

      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_description">
        <?php echo $form['description']->renderError() ?>
        <div>
          <?php echo $form['description']->renderLabel() ?>
          <div class="content">
            <?php echo $form['description']->render() ?>
          </div>
        </div>
      </div>

      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_name">
        <?php echo $form['logo']->renderError() ?>
        <div>
          <?php echo $form['logo']->renderLabel() ?>
          <div class="content">
            <?php echo $form['logo']->render() ?>
          </div>
        </div>
      </div>

      <?php echo $form->renderHiddenFields() ?>
      <input type="hidden" name="sf_method" value="PUT" />

    </fieldset>
    <ul class="sf_admin_actions">
      <li class="sf_admin_action_save"><input type="submit" value="<?php echo __('Save', array(), 'sf_admin') ?>"></li>
    </ul>
  </form>
</div>
