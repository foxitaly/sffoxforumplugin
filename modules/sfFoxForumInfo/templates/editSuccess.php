<?php use_helper('I18N', 'Date') ?>
<?php include_partial('sfFoxForumInfo/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit forum', array(), 'messages') ?></h1>

  <?php include_partial('sfFoxForumInfo/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('sfFoxForumInfo/form_header', array('forum' => $forum, 'form' => $form, )) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('sfFoxForumInfo/form', array('forum' => $forum, 'form' => $form, 'route' => $route)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('sfFoxForumInfo/form_footer', array('forum' => $forum, 'form' => $form)) ?>
  </div>
</div>
