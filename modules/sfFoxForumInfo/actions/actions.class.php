<?php

require_once dirname(__FILE__).'/../lib/BasesfFoxForumInfoActions.class.php';

/**
 * sfFoxForumInfo actions.
 *
 * @package    sfFoxForumPlugin
 * @subpackage sfFoxForumInfo
 * @author     Consulting Alpha
 * @version    SVN: $Id: actions.class.php 1432 2012-12-06 13:28:30Z garakkio $
 */
class sfFoxForumInfoActions extends BasesfFoxForumInfoActions
{
}
