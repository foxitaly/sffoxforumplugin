<?php

/**
 * Base components for the sfFoxForumPlugin sfFoxForumFrontend module.
 *
 * @package     sfFoxForumPlugin
 * @subpackage  sfFoxForumFrontend
 * @author      Consulting Alpha
 * @version     SVN: $Id: BasesfFoxForumFrontendActions.class.php 1487 2012-12-10 17:09:03Z root $
 */
abstract class BasesfFoxForumFrontendComponents extends sfComponents
{

  /**
   * checkConfiguration
   *
   */
  public function checkConfiguration()
  {

    $connection   = $this->dbConnection;
    $routePrefix  = $this->routePrefix;
    if (!isset($connection, $routePrefix))
    {
      throw new DomainException('Missing info in module.yml');
    }
    $aConnection = Propel::getConnection($connection);

    return Array(
      'connection'  => $connection,
      'routePrefix' => $routePrefix,
      'aConnection' => $aConnection
    );

  }

  /**
   * moderationList
   *
   * @params sfWebRequest $request
   *
   */
  public function executeModerationList(sfWebRequest $request)
  {
    $conf = $this->checkConfiguration();
 
    $this->dbConnection = $conf['aConnection'];
 
    $this->pager = FoxForumPostQuery::create()->
      filterPending()->
      orderByCreatedAt('desc')->
      paginate($request->getParameter('page', 1), 20, $this->dbConnection);
  }


  /**
   * Executes latestThread
   *
   * @params sfWebRequest $request
   *
   */

  public function executeLatestThread(sfWebRequest $request)
  {
    $conf = $this->checkConfiguration();

    $this->pager = FoxForumThreadQuery::create()->
      filterApproved($this->getUser()->isModerator())->
      orderByCreatedAt('desc')->
      paginate($request->getParameter('page', 1), 3, $conf['aConnection']);

  }


}
