<?php

/**
 * Base actions for the sfFoxForumPlugin sfFoxForumFrontend module.
 *
 * @package     sfFoxForumPlugin
 * @subpackage  sfFoxForumFrontend
 * @author      Consulting Alpha
 * @version     SVN: $Id: BasesfFoxForumFrontendActions.class.php 1693 2013-02-05 08:29:23Z mauro $
 */
abstract class BasesfFoxForumFrontendActions extends sfActions
{
  /**
   * Set needed variables
   *
   * @param sfRequest $request A request object
   */
  public function preExecute()
  {
    $module         = $this->getRequest()->getParameter('module');
    $this->connectionName = sfConfig::get('mod_' . strtolower($module) . '_db_connection_name');

    $this->routePrefix = sfConfig::get('mod_' . strtolower($module) . '_route_prefix');
    if (!isset($this->connectionName, $this->routePrefix))
    {
      throw new DomainException('Missing info in module.yml');
    }
    $this->dbConnection = Propel::getConnection($this->connectionName);
    sfConfig::set('mod_moderators', sfConfig::get('mod_' . strtolower($module) . '_forum_moderators'));
    sfConfig::set('mod_sendPostThread', sfConfig::get('mod_' . strtolower($module) . '_forum_sendPostThread'));
    sfConfig::set('mod_simpleUserOpenThread', sfConfig::get('mod_' . strtolower($module) . '_forum_simpleUserOpenThread'));
  }


  /**
   * Show threads
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forum = FoxForumQuery::create()->findOne($this->dbConnection);
    $this->forward404Unless($this->forum, 'forum not found');
    $this->pager = FoxForumThreadQuery::create()->
      filterApproved($this->getUser()->isModerator())->
      orderByCreatedAt('desc')->
      paginate($request->getParameter('page', 1), 5, $this->dbConnection);
  }

  /**
   * Insert new thread
   *
   * @param sfRequest $request A request object
   */
  public function executeNewThread(sfWebRequest $request)
  {
    if((!sfConfig::get('mod_sendPostThread', true) || !sfConfig::get('mod_simpleUserOpenThread', true) )&& !$this->getUser()->isModerator())
    {
      $this->setTemplate('forumClosed');
    }
    else
    {
      $this->form = new FoxForumThreadForm(null, array('user' => $this->getUser()));
      if ($request->isMethod(sfRequest::POST))
      {
        $this->form->bind($request->getParameter($this->form->getName()));
        if ($this->form->isValid())
        {
          $aThread = $this->form->insertNew($request->getRemoteAddress(), $this->dbConnection);
          $this->sendMailsModeratePending();
          $this->thankyouToggle($aThread);
          $this->redirectUnless($request->isXmlHttpRequest(), '@'. $this->routePrefix . '_forum');
        }
      }
    }
  }

  /**
   * Reply to a thread
   *
   * @param sfRequest $request A request object
   */
  public function executeThreadReply(sfWebRequest $request)
  {

    if(!sfConfig::get('mod_sendPostThread', true) && !$this->getUser()->isModerator())
    {
      $this->setTemplate('forumClosed');
    }
    else
    {
      $this->thread = $this->getRoute()->getObject();
      $this->form = new FoxForumPostForm(null, array('user' => $this->getUser()));
      if ($request->isMethod(sfRequest::POST))
      {
        $this->form->bind($request->getParameter($this->form->getName()));
        if ($this->form->isValid())
        {
          $aReply = $this->form->insertReply($request->getRemoteAddress(), $this->thread, $this->dbConnection);
          $this->thankyouToggle($aReply);
          $this->sendMailsModeratePending();
          $this->redirectUnless($request->isXmlHttpRequest(), $this->routePrefix . '_forum_read', $this->thread);
        }
      }
    }
  }

  /**
   * Reply to a post
   *
   * @param sfRequest $request A request object
   */
  public function executePostReply(sfWebRequest $request)
  {

    if(!sfConfig::get('mod_sendPostThread', true) && !$this->getUser()->isModerator())
    {
      $this->setTemplate('forumClosed');
    }
    else
    {

      $this->post = $this->getRoute()->getObject();
      $this->form = new FoxForumPostForm(null, array('user' => $this->getUser()));
      if ($request->isMethod(sfRequest::POST))
      {
        $this->form->bind($request->getParameter($this->form->getName()));
        if ($this->form->isValid())
        {
          $aComment = $this->form->insertComment($request->getRemoteAddress(), $this->post, $this->dbConnection);
          $this->sendMailsModeratePending();
          $this->thankyouToggle($aComment);
          $this->redirectUnless($request->isXmlHttpRequest(), $this->routePrefix . '_forum_read', $this->post->getThread($this->dbConnection));
        }
      }
    }
  }

 /**
  * Show a thread
  */
  public function executeRead()
  {
    $this->forum = FoxForumQuery::create()->findOne($this->dbConnection);
    $this->forward404Unless($this->forum, 'forum not found');
    $this->thread = $this->getRoute()->getObject();
    $this->forward404Unless($this->thread, 'thread not found');
   
    if(!$this->getUser()->isModerator())
    {
      $this->forward404Unless($this->thread->isApproved(), 'thread not approved');
    }

    $this->reply = $this->thread->getExpertReply($this->dbConnection);
    if (!empty($this->reply))
    {
      $this->comments = $this->reply->getComments($this->dbConnection);
    }
    $this->prev = $this->thread->getPrevThread($this->dbConnection);
    $this->next = $this->thread->getNextThread($this->dbConnection);
  }

  /**
   * Approve a thread
   */
  public function executeThreadApprove()
  {
    $this->forwardUnless($this->getUser()->isModerator(), sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
    $thread = $this->getRoute()->getObject();
    $thread->approve()->save($this->dbConnection);
    $this->sendMailsModerateApproveReject($thread,'ok');
    $this->redirect($this->routePrefix . '_forum');
  }

  /**
   * Reject a thread
   */
  public function executeThreadReject()
  {
    $this->forwardUnless($this->getUser()->isModerator(), sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
    $thread = $this->getRoute()->getObject();
    $thread->reject()->save($this->dbConnection);
    $this->sendMailsModerateApproveReject($thread,'ko');
    $this->redirect($this->routePrefix . '_forum');
  }

  /**
   * Approve a post
   */
  public function executePostApprove()
  {
    $this->forwardUnless($this->getUser()->isModerator(), sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
    $post = $this->getRoute()->getObject();
    $post->approve()->save($this->dbConnection);
    $this->sendMailsModerateApproveReject($post,'ok');
    $this->redirect($this->routePrefix . '_forum_read', $post->getThread($this->dbConnection));
  }

  /**
   * Reject a post
   */
  public function executePostReject()
  {
    $this->forwardUnless($this->getUser()->isModerator(), sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
    $post = $this->getRoute()->getObject();
    $post->reject()->save($this->dbConnection);
    $this->sendMailsModerateApproveReject($post,'ko');
    $this->redirect($this->routePrefix . '_forum_read', $post->getThread($this->dbConnection));
  }

  /**
   * Warnings
   */
  public function executeWarnings()
  {
    $this->forum = FoxForumQuery::create()->findOne($this->dbConnection);
    $this->forward404Unless($this->forum, 'forum not found');
    $this->setLayout(false);
  }

  /**
   * Thankyou
   */
  public function executeThankyou()
  {
  }


  /**
   * ThankyouToggle
   *
   * @Params Thread/Post $object
   *
   */
  private function thankyouToggle($object)
  {
    if($object)
    {
      $this->threadOrPost = $object;
      $request = $this->getRequest();
      if ($request->isXmlHttpRequest())
      {
        if($object->getModerator())
        {
          $this->setTemplate('thankyouModerator');
        }
        else
        {
          $this->setTemplate('thankyou');
        }
        return sfView::SUCCESS;
      }
    }
  }

  /**
   * send mails after thread or Post completed
   */
  private function sendMailsModeratePending()
  {
    $mailer       = $this->getMailer();
    $user         = $this->getUser();
    $moderators   = sfConfig::get('mod_moderators', array());
    $module       = $this->getRequest()->getParameter('module');

    $fromMail     = sfConfig::get('mod_' . strtolower($module) . '_emails_from');
    $fromName     = sfConfig::get('mod_' . strtolower($module) . '_emails_name');
    $toModerator  = sfConfig::get('mod_' . strtolower($module) . '_emails_from');
    $subjects     = sfConfig::get('mod_' . strtolower($module) . '_emails_subject');

    try
    {
      # MAIL TO USER
      $to = $user->getEmail();
      $text = "Grazie per aver scritto il tuo commento è in fase di moderazione";
      $message1 = $mailer->compose();
      $message1->setSubject($subjects['moderate_pending']);
      $message1->setFrom(array($fromMail => $fromName));
      $message1->setTo($to);
      $message1->setBody($text, 'text/html');
      $mailer->send($message1);

      # MAIL TO MODERATOR
      $subject  = $subjects['moderator'];
      $text     = "È stato inserito un nuovo post nel forum";
      $message2 = $mailer->compose();
      $message2->setFrom(array($fromMail => $fromName));
      $message2->setTo($toModerator);
      $message2->setBcc($moderators);
      $message2->setSubject($subject);
      $message2->setBody($text, 'text/html');
      $mailer->send($message2);
    }
    catch (Exception $e)
    {
    }
  }

  /**
   * send mails after thread or Post approved
   *
   * @params Thread/Post $obj
   * $params string $type (ok|ko)
   */
  private function sendMailsModerateApproveReject($obj, $type)
  {

    if($obj->getNotifyEmail())
    {

      $mailer       = $this->getMailer();
      $user         = $this->getUser();
      $moderators   = sfConfig::get('mod_moderators', array());
      $module       = $this->getRequest()->getParameter('module');

      $fromMail     = sfConfig::get('mod_' . strtolower($module) . '_emails_from');
      $fromName     = sfConfig::get('mod_' . strtolower($module) . '_emails_name');
      $toModerator  = sfConfig::get('mod_' . strtolower($module) . '_emails_from');
      $subjects     = sfConfig::get('mod_' . strtolower($module) . '_emails_subject');

      # MAIL TO USER
      $to = $obj->getEmail();

      if($to != '')
      {
        try
        {
          $text = ($type == 'ok') ? "Grazie per aver scritto il tuo commento è stato approvato" : "Spiacenti il tuo post non è stato approvato";
          $message1 = $mailer->compose();
          $message1->setSubject($subjects['moderate_'.$type]);
          $message1->setFrom(array($fromMail => $fromName));
          $message1->setTo($to);
          $message1->setBody($text, 'text/html');
          $mailer->send($message1);
        }
        catch(Exception $e)
        {}
      }
    }

  }

}
