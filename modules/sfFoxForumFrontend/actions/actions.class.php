<?php

require_once dirname(__FILE__).'/../lib/BasesfFoxForumFrontendActions.class.php';

/**
 * sfFoxForumFrontend actions.
 * 
 * @package    sfFoxForumPlugin
 * @subpackage sfFoxForumFrontend
 * @author     Consulting Alpha
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfFoxForumFrontendActions extends BasesfFoxForumFrontendActions
{
}
