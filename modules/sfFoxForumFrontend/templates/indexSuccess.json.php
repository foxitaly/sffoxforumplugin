<?php
use_helper('Text');

$array = array(
  'next'    => $pager->haveToPaginate() && $sf_request->getParameter('page') != $pager->getLastPage() ? url_for('@' . $routePrefix . '_forum?page=' . $pager->getNextPage()) : '',
  'threads' => array(),
);

foreach ($pager as $thread)
{

  $reply = $thread->getExpertReply($dbConnection->getRawValue());

  $array['threads'][$thread->getId()] = array(
    'nick'    => $thread->getNickname(),
    'date'    => format_date($thread->getCreatedAt()),
    'title'   => $thread->getTitle(),
    'text'    => truncate_text($thread->getMessage(), 200),
    'isMod'   => $thread->getModerator(),
    'url'     => url_for($routePrefix . '_forum_read', $thread),
    'numComm' => $thread->getModerator() || is_null($reply) ? 0 : $reply->countActiveDescendants($dbConnection->getRawValue()),
    'approve' => $sf_user->isModerator() && ($thread->isPending() || $thread->isRejected()) ? url_for($routePrefix . '_forum_thread_approve', $thread) : '',
    'reject'  => $sf_user->isModerator() && ($thread->isPending() || $thread->isApproved()) ?  url_for($routePrefix . '_forum_thread_reject', $thread) : '',
    'status'  => $thread->getStatusLabel(),
    'reply'   => null,
  );

  if(!is_null($reply))
  {
    $array['threads'][$thread->getId()]['reply'] = array(
      'nick'    => $reply->getNickname(),
      'date'    => format_date($reply->getCreatedAt()),
      'text'    => truncate_text($reply->getMessage(), 200),
      'isMod'   => $reply->getModerator(),
      'url'     => url_for($routePrefix . '_forum_read', $thread),
      'numComm' => $thread->getModerator() || is_null($reply ) ? 0 : $reply->countActiveDescendants($dbConnection->getRawValue()),
      'approve' => $sf_user->isModerator() && ($reply->isPending() || $reply->isRejected()) ? url_for($routePrefix . '_forum_post_approve', $reply) : '',
      'reject'  => $sf_user->isModerator() && ($reply->isPending() || $reply->isApproved()) ?  url_for($routePrefix . '_forum_post_reject', $reply) : '',
      'status'  => $reply->getStatusLabel()
    );
  }
  
}

echo json_encode($array);
