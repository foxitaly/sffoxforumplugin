<?php use_helper('Text') ?>
<?php use_javascript('/sfFoxForumPlugin/js/toggle.js') ?>

<?php $sf_response->setTitle($show . " - " . $sf_response->getTitle()) ?>

<?php $aDescription = $show->getMetaDescription() ? $show->getMetaDescription() : $show->getSynopsis() ?>
<?php $aKeywords    = ($show->getMetaKeywords())  ? $show->getMetaKeywords()    : null ?>
<?php $aImage       = url_for('@homepage', true) . url_for($show->getCover()) ?>
<?php $aType        = 'tv_show' ?>

<?php slot('metaGeneric') ?>
<?php include_partial('global/metaGeneric', array(
  'description' => $aDescription,
  'keywords'    => $aKeywords
)) ?>
<?php end_slot() ?>


<?php slot('ogGeneric') ?>
  <?php include_partial('global/metaOgGeneric', array(
    'ogDescription' => $aDescription,
    'ogImage'       => $aImage,
    'ogType'        => $aType
  )) ?>
<?php end_slot() ?>

<?php use_stylesheet('/css/forum/struttura.css') ?>
<?php use_stylesheet('/css/forum/temaForum.css') ?>
<?php use_stylesheet('/css/forum/temaForum-'. $sf_request->getParameter('show') .'.css') ?>

<script type="text/javascript">
  $(document).ready(function(){
    $(".ajax").colorbox();
    $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
  });
</script>

<script type="text/javascript">getNComments(<?php echo $show->getId() ?>);</script>

<?php include_partial('show/showCover', array('show' => $show)) ?>

<div class="module620 paddingLeft10 forum">
  <img width="250" src="/uploads/photo/<?php echo $forum->getLogo() ?>" title="Foto dell'esperto" alt="Foto dell'esperto" />

  <div class="testata">
    <?php
      include_partial('sfFoxForumFrontend/header', array(
        'routePrefix' => $routePrefix,
        'forum'       => $forum
      ));
    ?>
  </div>
</div>

<div class="module300 paddingRight10 last">
  <?php include_partial('show/showColDx', array('show'=>$show,'noFollowUs'=>true, 'noFirstAdv'=>false, 'noTvGuide'=>true, 'noShowExtraBox'=>true, 'noLastVideo'=>true, 'noLastNews'=>true, 'noGalleries'=>true, 'noForum'=>true, 'noLastAdv'=>true,)) ?>
</div>

<div class="clear"></div>

<div class="module940 floatnone marginAuto forum" id="contenuto">

  <?php if (!$sf_user->isAuthenticated()): ?>
  <div class="forumAlert"><strong>Per poter scrivere domande e commenti occorre <a id="gLogin" href="<?php echo url_for('sfGigyaLogin') ?>" class="cboxElement">eseguire il login</a></strong></div>
  <?php endif ?>

  <ul class="level1">
    <li class="user">
      <div class="author">Creata da: <?php echo $thread->getNickname() ?> il <?php echo format_date($thread->getCreatedAt()) ?></div>
      <h4><?php echo $thread->getTitle() ?></h4>
      <p><?php echo $thread->getMessage() ?></p>
      <?php if (empty($reply)): ?>

      <?php if ($sf_user->isModerator() || $thread->getModerator()): ?>
      <a href="<?php echo url_for($routePrefix . '_forum_thread_reply', $thread) ?>" class="btn ajax left">SCRIVI UNA RISPOSTA</a>
      <?php else: ?>
      (Nessuna risposta)
      <?php endif ?>

      <?php else: ?>
      <ul class="level2">
        <li class="<?php echo $reply->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $reply->getStatusLabel();?>">
          <div class="author">Creata da: <?php echo $reply->getNickname() ?></div>
          <p><?php echo $reply->getMessage() ?></p>
          <?php if ($sf_user->isAuthenticated()): ?>
          <a href="<?php echo url_for($routePrefix . '_forum_post_reply', $reply) ?>" class="btn ajax left">SCRIVI UN COMMENTO</a>
          <?php else: ?>
          <a href="#" title="scrivi un commento" class="btn ajax left inactive">SCRIVI UN COMMENTO</a>
          <?php endif ?>
          <div class="commenti"><span><?php echo count($comments) ?></span>&nbsp;<a href="javascript:void(0)" title="leggi i commenti" id="expandFirst">Commenti</a></div>
          <div class="clear"></div>
          <?php if (!empty($comments)): ?>
          <ul class="level3">
            <?php foreach ($reply->getActiveChildren($dbConnection->getRawValue(),$sf_user->isModerator()) as $comment): ?>
            <li class="<?php echo $comment->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $comment->getStatusLabel();?>">
              <?php include_partial('sfFoxForumFrontend/post', array('post' => $comment, 'routePrefix' => $routePrefix)) ?>
              <?php if ($comment->hasChildren()): ?>
              <ul class="level4">
                <?php foreach ($comment->getActiveChildren($dbConnection->getRawValue(),$sf_user->isModerator()) as $subcomment): ?>
                <li class="<?php echo $subcomment->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $subcomment->getStatusLabel();?>">
                  <?php include_partial('sfFoxForumFrontend/post', array('post' => $subcomment, 'routePrefix' => $routePrefix, 'noCorpo' => false, 'noReply' => true)) ?>
                  <?php if ($subcomment->hasChildren()): ?>
                  <ul class="level5">
                    <?php foreach ($subcomment->getActiveChildren($dbConnection->getRawValue(),$sf_user->isModerator()) as $subsubcomment): ?>
                    <li class="<?php echo $subsubcomment->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $subsubcomment->getStatusLabel();?>">
                      <?php include_partial('sfFoxForumFrontend/post', array('post' => $subsubcomment, 'routePrefix' => $routePrefix, 'noReply' => true)) ?>
                    </li>
                    <?php endforeach ?>
                  </ul>
                  <?php include_partial('sfFoxForumFrontend/post', array('post' => $subcomment, 'routePrefix' => $routePrefix, 'noCorpo' => true, 'noReply' => false)) ?>
                  <?php endif ?>
                </li>
                <?php endforeach ?>
              </ul>
              <?php endif ?>
            </li>
            <?php endforeach ?>
          </ul>
          <?php endif ?>
        </li>
      </ul>
      <?php endif ?>
    </li>
  </ul>

  <?php if ($prev): ?>
  <a href="<?php echo url_for($routePrefix . '_forum_read', $prev) ?>" title="Discussione precedente" class="left arrow btn">&laquo; PRECENDENTE</a>
  <?php endif ?>
  <a href="<?php echo url_for('@' . $routePrefix . '_forum') ?>" title="Torna alla home" class="center arrow btn">FORUM HOME</a>
  <?php if ($next): ?>
  <a href="<?php echo url_for($routePrefix . '_forum_read', $next) ?>" title="Discussione successiva" class="right arrow btn">SUCCESSIVA &raquo;</a>
  <?php endif ?>
  <div class="clear"></div>

</div>

<div class="clear"></div>
