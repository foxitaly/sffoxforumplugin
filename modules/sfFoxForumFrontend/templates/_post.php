<?php $noCorpo = (!isset($noCorpo)) ? false : $noCorpo ?>
<?php if(!$noCorpo): ?>
<div class="author">Creato da: <?php echo $post->getNickname() ?></div>
<p><?php echo $post->getMessage() ?></p>

<?php include_partial('sfFoxForumFrontend/approveReject', array('routePrefix' => $routePrefix, 'element' => $post, 'type' => 'post')) ?>

<?php endif ?>
<?php if (empty($noReply) && $sf_user->isAuthenticated() && ($sf_user->isModerator() || $post->getModerator())): ?>
<a href="<?php echo url_for($routePrefix . '_forum_post_reply', $post) ?>" class="btn ajax left">SCRIVI UN COMMENTO</a>
<div class="clear"></div>
<?php else: ?>
<a href="#" title="scrivi un commento" class="btn ajax left inactive">SCRIVI UN COMMENTO</a>
<div class="clear"></div>
<?php endif ?>
