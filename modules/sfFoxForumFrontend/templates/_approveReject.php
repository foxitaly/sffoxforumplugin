<?php if ($sf_user->isModerator()): ?>
  <?php if ($element->isPending() || $element->isRejected()): ?>
    <a href="<?php echo url_for($routePrefix . '_forum_' . $type . '_approve', $element) ?>" title="Accetta" class="buttonAccetta">PUBBLICA</a>
  <?php endif ?>
  <?php if ($element->isPending() || $element->isApproved()): ?>
    <a href="<?php echo url_for($routePrefix . '_forum_' . $type. '_reject', $element) ?>" title="Rifiuta" class="buttonRifiuta">DEPUBBLICA</a>
  <?php endif ?>
    <div class="clear"></div>
<?php endif ?>
