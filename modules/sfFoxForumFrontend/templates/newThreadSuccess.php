<div class="size18 fontBold alignCenter">Scrivi una nuova domanda</div>
<div style="border-bottom:1px solid #E4E4E4; margin:10px 0px"></div>
<div class="containerForm">
  <form method="post" action="<?php echo url_for('@' . $routePrefix . '_forum_new_thread') ?>">

  <label for="fox_forum_thread_title">Titolo</label>
  <?php echo $form['title']->renderError(); ?>
  <?php echo $form['title']; ?>


  <label for="fox_forum_thread_message">Messaggio</label>
  <?php echo $form['message']->renderError(); ?>
  <?php echo $form['message']; ?>

  <label>&nbsp;</label>
  <?php echo $form['notify_email']; ?>&nbsp; Il forum è moderato. Avvisami via Email quando il mio post verr&agrave; pubblicato
  
  <?php echo $form->renderHiddenFields();?>

  <button id="sfFoxForumThread" type="submit">INVIA DOMANDA</button>
  </form>
</div>
