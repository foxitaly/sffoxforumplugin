<?php if ($sf_user->isModerator()): ?>

<?php if($pager->count()): ?>

<h3 class="titolo">COMMENTI DA MODERARE</h3>
  
<ul class="level1">

  <li class="user moderate-pending">

    <?php foreach($pager->getResults() as $post): ?>
    <?php $aThread = $post->getThread($dbConnection->getRawValue());?>

    <div class="author">Creata da: <?php echo $post->getNickname(); ?> il <?php echo format_date($post->getCreatedAt());?></div>
    <h4>COMMENTO ALLA DISCUSSIONE "<?php echo $aThread->getTitle(); ?>"</h4>
    <a href="<?php echo url_for($routePrefix . '_forum_read', $aThread) ?>#<?php echo $post->getId();?>" title="Commento al thread" class="buttonMore" style="margin:10px 0px">MODERA</a>
    
    <div class="separator"></div>

    <?php endforeach; ?>
    
  </li>

</ul>

<?php endif; ?>

<?php endif; ?>

