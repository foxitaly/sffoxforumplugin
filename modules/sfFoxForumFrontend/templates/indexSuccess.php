<?php use_helper('Text') ?>
<?php use_javascript('/sfFoxForumPlugin/js/forum.js') ?>

<?php $sf_response->setTitle($show . " - " . $sf_response->getTitle()) ?>

<?php $aDescription = ($show->getMetaDescription()) ? $show->getMetaDescription() : $show->getSynopsis() ?>
<?php $aKeywords    = ($show->getMetaKeywords())    ? $show->getMetaKeywords()    : null ?>
<?php $aImage       = url_for('@homepage', true) . url_for($show->getCover()) ?>
<?php $aType        = 'tv_show' ?>

<?php slot('metaGeneric') ?>
  <?php include_partial('global/metaGeneric', array(
    'description' => $aDescription,
    'keywords'    => $aKeywords
  )) ?>
<?php end_slot() ?>


<?php slot('ogGeneric') ?>
  <?php include_partial('global/metaOgGeneric', array(
    'ogDescription' => $aDescription,
    'ogImage'       => $aImage,
    'ogType'        => $aType
    )) ?>
<?php end_slot() ?>

<?php use_stylesheet('/css/foxlife/custom/' . $sf_request->getParameter('show') . '/style.css') ?>
<?php use_stylesheet('/css/foxlife/custom/' . $sf_request->getParameter('show') . '/extra.css') ?>
<?php use_stylesheet('/css/forum/temaForum.css') ?>
<?php use_stylesheet('/css/forum/temaForum-'. $sf_request->getParameter('show') .'.css') ?>

<script type="text/javascript">
  $(document).ready(function(){
    $(".ajax").colorbox();
    $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
  });
</script>

<script type="text/javascript">getNComments(<?php echo $show->getId() ?>);</script>

<?php include_partial('show/showCover', compact('show')) ?>

<?php if (!$sf_user->isAuthenticated()): ?>
<div class="forumAlert" style="margin:10px;"><strong>Per poter scrivere domande e commenti occorre <a id="gLogin" href="<?php echo url_for('sfGigyaLogin') ?>" class="cboxElement">eseguire il login</a></strong></div>
<?php endif ?>

<div class="module620 forum paddingLeft10" id="contenuto">

  <img width="250" src="/uploads/photo/<?php echo $forum->getLogo() ?>" title="Foto dell'esperto" alt="Foto dell'esperto" />
  <div class="testata">
    <?php
      include_partial('sfFoxForumFrontend/header', array(
        'routePrefix' => $routePrefix,
        'forum'       => $forum
      ));
    ?>
  </div>
</div>

<div class="module300 paddingRight10 last">
<?php include_partial('show/showColDx', array('show' => $show, 'noFollowUs' => true, 'noFirstAdv' => false, 'noTvGuide' => true, 'noShowExtraBox' => true, 'noLastVideo' => true, 'noLastNews' => true, 'noGalleries' => true, 'noForum' => true, 'noLastAdv' => true)) ?>
</div>
<div class="clear"></div>
<div class="module940 floatnone marginAuto forum">

	<!-- visibile solo dall'amministratore -->
  <?php include_component('sfFoxForumFrontend','moderationList', Array('dbConnection' => $connectionName, 'routePrefix' => $routePrefix)); ?>
    
  <!-- visibile solo dall'amministratore -->


  <h3 class="titolo">ULTIME DISCUSSIONI</h3>

  <ul class="level1">

    <?php /* ATTENZIONE: se si modifica questa parte di layout, modificare anche forum.js in modo coerente */ ?>

    <?php foreach ($pager as $thread): ?>

    <li class="<?php echo $thread->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $thread->getStatusLabel() ?>">
        <div class="author">Creata da: <?php echo $thread->getNickname() ?> il <?php echo format_date($thread->getCreatedAt()) ?></div>
        <h4><a href="<?php echo url_for($routePrefix . '_forum_read', $thread) ?>" title="Leggi tutto"><?php echo $thread->getTitle() ?></a></h4>
        <p><?php echo truncate_text($thread->getMessage(), 200) ?></p>
        <?php include_partial('sfFoxForumFrontend/approveReject', array('routePrefix' => $routePrefix, 'element' => $thread, 'type' => 'thread')) ?>
        <div class="clear"></div>

      	<?php $reply = $thread->getExpertReply($dbConnection->getRawValue()) ?>

        <?php if(!is_null($reply)): ?>

		    <ul class="level2">
      		<li class="<?php echo $reply->getModerator() ? 'admin' : 'user' ?> moderate-<?php echo $reply->getStatusLabel();?>">
                <div class="author">Creata da: <?php echo $reply->getNickname() ?></div>
                <p><?php echo truncate_text($reply->getMessage(), 200) ?></p>
                
                <div class="commenti">
                <?php 
                  include_partial('sfFoxForumFrontend/countComments', Array(
                    'thread'        => $thread,
                    'reply'         => $reply,
                    'dbConnection'  => $dbConnection
                  )); 
                ?>
                </div>
                <div class="module140 right">
                	<a href="<?php echo url_for($routePrefix . '_forum_read', $thread) ?>" title="Leggi tutto" class="btn">LEGGI E COMMENTA&nbsp;&rsaquo;</a>
                </div>
                <div class="clear"></div>
            </li>
      	</ul>

        <?php else: ?>

		    <ul class="level2">
          <li class="admin moderate-<?php echo $thread->getStatusLabel();?>">
            <div class="commenti">
              <?php 
                include_partial('sfFoxForumFrontend/countComments', Array(
                  'thread'        => $thread,
                  'reply'         => $reply,
                  'dbConnection'  => $dbConnection
                )); 
              ?>
            </div>
            <div class="module140 right">
              <a href="<?php echo url_for($routePrefix . '_forum_read', $thread) ?>" title="Leggi tutto" class="btn">LEGGI E COMMENTA&nbsp;&rsaquo;</a>
            </div>
            <div class="clear"></div>
          </li>
        </ul>

    
        <?php endif; ?>

    </li>

    <?php endforeach ?>

  </ul>

  <?php if ($pager->haveToPaginate() && $sf_request->getParameter('page') != $pager->getLastPage()): ?>
  <a href="<?php echo url_for('@' . $routePrefix . '_forum?page=' . $pager->getNextPage()) ?>" title="Altre discussioni" class="btn paginazione">ALTRE DISCUSSIONI</a>
  <?php endif ?>

</div>

<div class="clear"></div>
