<h1><?php echo $forum->getTitle() ?></h1>
<h2><?php echo $forum->getSubtitle() ?></h2>
<?php echo $forum->getDescription(ESC_RAW); ?>
<br />
<?php if ($sf_user->isAuthenticated()): ?>
<a href="<?php echo url_for('@' . $routePrefix . '_forum_new_thread') ?>" class="btn new ajax left">FAI UNA DOMANDA</a>
<?php else: ?>
<a href="#" title="Fai una domanda" class="btn new ajax left disabled">FAI UNA DOMANDA</a>
<?php endif ?>
<a href="<?php echo url_for('@' . $routePrefix . '_forum_warnings');?>" title="Avvertenze" class="iframe avvertenze">AVVERTENZE</a>

