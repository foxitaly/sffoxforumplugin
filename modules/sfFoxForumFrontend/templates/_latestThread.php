<div class="forumPanel">
	<div class="testata">
      <h3><span class="titolo">FORUM</span><br /><span class="sottotitolo">ULTIME DOMANDE</span></h3>
  </div>
  <div class="corpo">
    <ul>
      <?php foreach($pager as $thread): ?>
      <li>
        <div class="immagine">
          <span class="giorno"><?php echo format_date($thread->getCreatedAt(),'EEE') ?></span><br />
          <span class="giornomese"><?php echo format_date($thread->getCreatedAt(),'dd/MMM') ?></span>
        </div>
        <div class="descrizione">
          <div class="testo">scritto da <strong><?php echo $thread->getNickname() ?></strong></div>
          <h4><a href="<?php echo url_for($routePrefix . '_forum_read', $thread) ?>" title="<?php echo $thread->getTitle(); ?>"><?php echo $thread->getTitle()?></a></h4>
        </div>
        <div class="clear"></div>
      </li>
      <?php endforeach; ?>
    
    </ul>
    <!--a href="#" title="Fai una domanda" class="button">FAI UNA DOMANDA</a-->
    <a href="<?php echo url_for($routePrefix . '_forum'); ?>" title="Le altre domande" class="buttonForum">LE ALTRE DOMANDE</a>
	</div>
</div>
